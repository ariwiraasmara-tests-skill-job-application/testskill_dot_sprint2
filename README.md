# testskill_dot_sprint2

# Gunakan Postman
Untuk bisa menggunakan API Endpoint berikut, diharapkan untuk login terlebih dahulu
1. Gunakan url localhost/testskill_dot_sprint2/public/api/login dengan metode POST, user = thewitch@hell.com, pass = asd123

Setelah login, user dapat:
1. akses endpoint untuk mencari data detil province dan kota
2. Gunakan url testskill_dot_sprint2/public/api/sp/{$id} untuk mencari data detil province dari Rajaongkir
3. Gunakan url testskill_dot_sprint2/public/api/sc/{$id} untuk mencari data detil city dari Rajaongkir
4. Gunakan url testskill_dot_sprint2/public/api/logout untuk logout
