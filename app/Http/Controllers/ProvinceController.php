<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ProvinceController extends Controller {
    //

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request, $id) {
        //
        $value = $request->cookie('name');
        if( isset($value) ) {
            $response = Http::withHeaders(['key'=>'0df6d5bf733214af6c6644eb8717c92c'])
                        ->get('https://api.rajaongkir.com/starter/province?id='.$id);
            //return $response['rajaongkir']['results'];
            $status = $response->getStatusCode();
            //return $response->json();
            if($status == 200)  {
            return response()->json(['msg'      => 'Search Province Success!',
                                    'success'  => 1,
                                    'data'     => $response['rajaongkir']['results']],
                                    $status);
            }
            return response()->json(['msg' => 'Search Province Fail!', 'success' => 0], $status);
        }
        return response()->json(['msg' => "Unauthorized! You're not have permission to be here! Get out!", 'success' => 0], 401);
    }
}
