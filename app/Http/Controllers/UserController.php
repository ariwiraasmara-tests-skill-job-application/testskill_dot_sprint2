<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Str;
use App\Http\Requests;
use App\Models\User;
use Cookie;

class UserController extends Controller {
    //
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request) {
        //
        $user = User::where('email', '=', $request->user)->first();
        if(!$user) return response()->json(['msg' => 'Login Fail! User Not Found!', 'success' => 0], 400);
        if (Hash::check($request->pass, $user->password)) {
            // The passwords match...
            $response = new Response('Test Skill Sprint 2');
            $response->withCookie(cookie('name', $request->user, 24 * 60));
            setcookie('token', Hash::make(Str::random(100000).rand(0, 999999)), time() + (1 * 24 * 60 * 60), "/"); // 1 * 24 * 60 * 60 = 1 day
            setcookie('name', Hash::make($request->user), time() + (1 * 24 * 60 * 60), "/"); // 1 * 24 * 60 * 60 = 1 day
            return response()->json(['msg' => "You're Login!", 'success' => 1, 'response' => $response], 200);
        }
        return response()->json(['msg' => 'Login Fail! Password unmatched!', 'success' => 0], 400);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request) {
        setcookie("token", "", time() - (1000 * 24 * 60 * 60), '/');
        setcookie("name", "", time() - (1000 * 24 * 60 * 60), '/');
        return response()->json(['msg' => "You're Logout! User Auth Has Been Removed!", 'success' => 1], 200);
    }
}
