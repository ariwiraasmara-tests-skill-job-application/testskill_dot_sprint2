<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', '\App\Http\Controllers\UserController@login');
Route::get('/logout', '\App\Http\Controllers\UserController@logout');
Route::get('/sp/{id}', '\App\Http\Controllers\ProvinceController@search');
Route::get('/sc/{id}', '\App\Http\Controllers\CityController@search');
